﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebjetMovies.Services;

namespace WebjetMovies.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var service = new MovieService();
            return View(service.Movies);
        }
    }
}