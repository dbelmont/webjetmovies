﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Caching;
using WebjetMovies.Models;

namespace WebjetMovies.Services
{
    public class MovieService : IMovieService
    {
        static MemoryCache _cache = MemoryCache.Default;
        readonly HttpClient _httpClient;
        int _retryNumber = 0;

        public IEnumerable<Movie> Movies
        {
            get
            {
                if (!_cache.Any(i => i.Key == "movies")) BuildCache();
                return (IEnumerable<Movie>)_cache.Get("movies");
            }
        }

        public MovieService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("http://webjetapitest.azurewebsites.net/");
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.Add("x-access-token", "sjd1HfkjU83ksdsm3802k");
        }

        public void BuildCache()
        {
            if (_cache.Any(i => i.Key == "movies")) _cache.Remove("movies");

            var movieService = new MovieService();
            var movies = ConvertWebjetMoviesToMovies(movieService.LoadMovies());
            var cachePolicy = new CacheItemPolicy { SlidingExpiration = TimeSpan.FromHours(1), RemovedCallback = (args) => new MovieService().BuildCache() };
            _cache.Add(new CacheItem("movies", movies), new CacheItemPolicy());
        }

        public IList<Movie> ConvertWebjetMoviesToMovies(IList<WebjetMovie> webjetMovies)
        {
            var movies = webjetMovies.Select(m => new Movie { Title = m.Title, Poster = m.Poster, Plot = m.Plot, Language = m.Language })
                .Distinct(new MovieComparer())
                .ToArray();
            for (var i = 0; i < movies.Length; i++)
            {
                movies[i].Availability.AddRange(
                    webjetMovies
                    .Where(m => m.Title == movies[i].Title)
                    .Select(m => new Movie.MovieAvailability
                    {
                        Provider = m.Provider,
                        Price = m.Price,
                        Id = m.Id
                    })
                    .OrderBy(m => m.Price)
                );
            }

            return movies.ToList();
        }

        public IList<WebjetMovie> LoadMovies()
        {
            var cinemaWorldMovies = LoadMovies(MovieProvider.CinemaWorld);
            var filmWorldMovies = LoadMovies(MovieProvider.FilmWorld);
            var movies = cinemaWorldMovies.Union(filmWorldMovies).ToList();
            return movies.Select(LoadMovieDetails).ToList();
        }

        public List<WebjetMovie> LoadMovies(MovieProvider provider)
        {
            try
            {
                var response = _httpClient.GetAsync(String.Format("api/{0}/movies", provider)).Result;
                if (response.IsSuccessStatusCode)
                {
                    _retryNumber = 0;
                    var webjetMovies = new List<WebjetMovie>();
                    webjetMovies.AddRange(response.Content.ReadAsAsync<WebjetMovieArray>().Result.Movies);
                    webjetMovies.ForEach(m => m.Provider = provider);
                    return webjetMovies;
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                    return LoadMovies(provider);
                }
            }
            catch (Exception ex)
            {
                if (_retryNumber++ >= 10) throw new Exceptions.ServiceRequestException(ex);
                return LoadMovies(provider);
            }
        }

        public WebjetMovie LoadMovieDetails(WebjetMovie movie)
        {
            try
            {
                var response = _httpClient.GetAsync(String.Format("api/{0}/movie/{1}", movie.Provider, movie.Id)).Result;
                if (response.IsSuccessStatusCode)
                {
                    _retryNumber = 0;
                    var result = response.Content.ReadAsAsync<WebjetMovie>().Result;
                    result.Poster = GetBase64Poster(result.Poster);
                    result.Provider = movie.Provider;
                    return result;
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                    return LoadMovieDetails(movie);
                }
            }
            catch (Exception ex)
            {
                if (_retryNumber++ >= 10) throw new Exceptions.ServiceRequestException(ex);
                return LoadMovieDetails(movie);
            }
        }

        private string GetBase64Poster(string posterUrl)
        {
            using (var webClient = new System.Net.WebClient())
            {
                try
                {
                    byte[] data = webClient.DownloadData(posterUrl);
                    var base64Data = Convert.ToBase64String(data);
                    return String.Format("data:image/gif;base64,{0}", base64Data);
                }
                catch
                {
                    return posterUrl;
                }
            }
        }
    }
}