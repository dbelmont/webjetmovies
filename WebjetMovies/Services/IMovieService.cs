﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebjetMovies.Models;

namespace WebjetMovies.Services
{
    public interface IMovieService
    {
        System.Collections.Generic.IEnumerable<Models.Movie> Movies { get; }
        void BuildCache();
        IList<Movie> ConvertWebjetMoviesToMovies(IList<WebjetMovie> webjetMovies);
        IList<WebjetMovie> LoadMovies();
        List<WebjetMovie> LoadMovies(MovieProvider provider);
        WebjetMovie LoadMovieDetails(WebjetMovie movie);
    }
}