﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebjetMovies.Models
{
    public class Movie
    {
        public string Title { get; set; }
        public string Poster { get; set; }
        public string Plot { get; set; }
        public string Language { get; set; }
        public List<MovieAvailability> Availability { get; set; }

        public Movie()
        {
            Availability = new List<MovieAvailability>();
        }

        public struct MovieAvailability
        {
            public string Id { get; set; }
            public MovieProvider Provider { get; set; }
            public float Price { get; set; }
        }

        public override int GetHashCode()
        {
            return Title.ToLowerInvariant().GetHashCode();
        }
    }

    public class MovieComparer : IEqualityComparer<Movie>
    {
        public bool Equals(Movie x, Movie y)
        {
            return StringComparer.InvariantCultureIgnoreCase.Compare(x.Title, y.Title) == 0;
        }

        public int GetHashCode(Movie obj)
        {
            return obj.GetHashCode();
        }
    }
}