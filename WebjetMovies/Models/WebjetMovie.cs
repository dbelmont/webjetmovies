﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebjetMovies.Models
{
    public class WebjetMovieArray
    {
        public WebjetMovie[] Movies { get; set; }
    }

    public class WebjetMovie
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Poster { get; set; }
        public string Plot { get; set; }
        public string Language { get; set; }
        public MovieProvider Provider { get; set; }
        public float Price { get; set; }
    }
}