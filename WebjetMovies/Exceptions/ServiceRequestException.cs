﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebjetMovies.Exceptions
{
    public class ServiceRequestException : Exception
    {
        public ServiceRequestException(Exception innerException) : base("Sorry... There was a problem while quering the Webjet Movies service.", innerException) {}
    }
}