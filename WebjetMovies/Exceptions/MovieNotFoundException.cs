﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebjetMovies.Exceptions
{
    public class MovieNotFoundException : Exception
    {
        public MovieNotFoundException(string title, Exception innerException) : base(String.Format("The movie '{0}' was not found.", title)) { }
        public MovieNotFoundException(string title) : this(title, null) { }
    }
}