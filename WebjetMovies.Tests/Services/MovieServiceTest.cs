﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebjetMovies.Models;
using WebjetMovies.Services;

namespace WebjetMovies.Tests.Services
{
    [TestClass]
    public class MovieServiceTest
    {
        [TestMethod]
        public void BuildCacheTest()
        {
            IMovieService movieSearch = new MovieService();
            movieSearch.BuildCache();
            Assert.IsTrue(movieSearch.Movies.Count() == 7);
        }

        [TestMethod]
        public void ConvertWebjetMoviesToMoviesTest()
        {
            var webjetMovies = new List<WebjetMovie>
            {
                new WebjetMovie { Title = "SourceCode", Plot = "A soldier wakes up in someone else's body and discovers he's part of an experimental government program to find the bomber of a commuter train. A mission he has only 8 minutes to complete.", Language = "English", Id = "cw123", Price = 25.19f, Provider = MovieProvider.CinemaWorld },
                new WebjetMovie { Title = "SourceCode", Plot = "A soldier wakes up in someone else's body and discovers he's part of an experimental government program to find the bomber of a commuter train. A mission he has only 8 minutes to complete.", Language = "English", Id = "fw123", Price = 22.05f, Provider = MovieProvider.FilmWorld }
            };

            var movies = new MovieService().ConvertWebjetMoviesToMovies(webjetMovies);

            Assert.IsTrue(movies.Count() == 1);
            Assert.IsTrue(movies.First().Availability.Count() == 2);
        }

        [TestMethod]
        public void LoadMoviesTest()
        {
            IMovieService service = new MovieService();
            var movies = service.LoadMovies();

            Assert.IsTrue(movies.Count() == 13);
        }

        [TestMethod]
        public void LoadMoviesByProviderTest()
        {
            IMovieService service = new MovieService();
            var cinemaWorldMovies = service.LoadMovies(Models.MovieProvider.CinemaWorld);
            var filmWorldMovies = service.LoadMovies(Models.MovieProvider.FilmWorld);

            Assert.IsTrue(cinemaWorldMovies.Count() == 7);
            Assert.IsTrue(filmWorldMovies.Count() == 6);
        }

        [TestMethod]
        public void LoadMovieDetailsTest()
        {
            IMovieService service = new MovieService();
            var cinemaWorldMovies = service.LoadMovies(Models.MovieProvider.CinemaWorld);
            var movie = cinemaWorldMovies.First();
            var updatedMovie = service.LoadMovieDetails(movie);

            Assert.IsTrue(movie.Price == 0);
            Assert.IsTrue(updatedMovie.Price == 123.5);
        }
    }
}